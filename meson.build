# Copyright © 2019 Intel Corporation

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

project(
  'apitrace',
  ['cpp'],
  meson_version : '>= 0.51',
  subproject_dir : 'thirdparty',
  default_options : ['cpp_std=c++11'],
)

if host_machine.system() == 'darwin'
  add_languages('objcpp')
endif

cpp = meson.get_compiler('cpp')

if host_machine.system() != 'windows'
  if not cpp.compiles('__thread int i; int main() { return 0; }', name : 'HAVE_TLS')
    error('C++ compiler does not support the __thread keyword.')
  endif
endif

add_project_arguments([
                       '-DAPITRACE_PYTHON_EXECUTABLE="python"',
                       '-DAPITRACE_PROGRAMS_INSTALL_DIR="' + get_option('bindir') + '"',
                       '-DAPITRACE_SCRIPTS_INSTALL_DIR="./scripts"',
                       '-DAPITRACE_WRAPPERS_INSTALL_DIR="' + get_option('libdir') + '/apitrace/wrappers"',
                      ],
                      language : 'cpp')

prog_py = import('python').find_installation()

dep_null = dependency('', required : false)
dep_threads = dependency('threads')
dep_qt = dependency('qt5', modules : ['widgets', 'network'], required : get_option('gui'))

dep_procps = dep_null
if host_machine.system() not in ['windows', 'darwin'] and not get_option('all_static')
  dep_procps = dependency('libprocps')
endif

dep_x11 = dep_null
if host_machine.system() == 'windows'
  add_project_arguments(['-D_WIN32_WINNT=0x0602', '-DWINVER=0x0602'], language : 'cpp')
elif host_machine.system() == 'darwin'
  error('TODO')
else
  dep_x11 = dependency('x11', required : get_option('x11'))
  if dep_x11.found()
    add_project_arguments(['-DHAVE_X11'], language : 'cpp')
  endif
endif

dep_waffle = dep_null
if get_option('egl')
  dep_waffle = dependency('waffle-1', fallback :['waffle', 'ext_waffle'], required: false)
endif

add_project_arguments('-D__STDC_LIMIT_MACROS', '-D__STDC_FORMAT_MACROS', language : 'cpp')

if cpp.get_argument_syntax() == 'msvc'
  add_project_arguments(
    '-D_USE_MATH_DEFINES',
    '-DNOMINMAX',
    '-D_CRT_SECURE_NO_DEPRECATE',
    '-D_CRT_SECURE_NO_WARNINGS',
    '-D_CRT_NONSTDC_NO_WARNINGS',
    '-D_SCL_SECURE_NO_DEPRECATE',
    '-D_SCL_SECURE_NO_WARNINGS',
    '-wd4018', # signed/unsigned mismatch
    '-wd4063', # not a valid value for switch of enum
    '-wd4100', # unreferenced formal parameter
    '-wd4127', # conditional expression is constant
    '-wd4244', # conversion from 'type1' to 'type2', possible loss of data
    '-wd4267', # conversion from 'type1' to 'type2', possible loss of data
    '-wd4505', # unreferenced local function has been removed
    '-wd4512', # assignment operator could not be generated
    '-wd4577', # 'noexcept' used with no exception handling mode specified
    '-wd4800', # forcing value to bool 'true' or 'false' (performance warning)
    language : ['cpp']
  )
elif cpp.get_argument_syntax() == 'gcc'
  add_project_arguments(
    cpp.get_supported_arguments([
      '-Wno-sign-compare',
      '-fno-strict-aliasing',
      '-Wno-non-virtual-dtor',
    ]),
    language : ['cpp']
  )

  # TODO: mingw specfiic bits
  if get_option('sse4_2')
    add_project_arguments('-DENABLE_SSE42', '-msse4.2', langauge : ['cpp'])
  endif
endif

# TODO: mingw and all_static
# TODO: framepointers

if host_machine.system() == 'windows'
  vp = cpp.sizeof('void *') > 4
  if cpp.get_argument_syntax() == 'msvc'
    add_project_arguments(
      '-bigobj',
      language : 'cpp'
    )
    add_project_link_arguments(
      '/NXCOMPAT',
      '/DYNAMICBASE',
      language : 'cpp'
    )
    if vp
      add_project_link_arguments('/LARGEADDRESSAWARE', language : 'cpp')
    endif
  elif cpp.get_argument_syntax() == 'gcc'
    add_project_arguments(
      '-Wa,-mbig-obj',
      language : 'cpp'
    )
    add_project_link_arguments(
      '-Wl,--nxcompat',
      '-Wl,--dynamicbase',
      language : 'cpp'
    )
    if vp and host_machine.system() != 'windows'
      add_project_link_arguments('-Wl,--large-address-aware', language : 'cpp')
    endif
  endif
elif host_machine.system() != 'darwin'
  add_project_arguments('-D_GNU_SOURCE', language : 'cpp')
endif

cmake = import('cmake')

dep_dl = cpp.find_library('dl', required : false)
dep_snappy = dependency('Snappy', fallback : ['google-snappy', 'snappy_dep'])
dep_zlib = dependency('zlib', version : '>= 1.2.6', fallback : ['zlib', 'zlib_dep'])
dep_png = dependency('libpng', fallback : ['libpng', 'png_dep'])
dep_crc32c = subproject('crc32c').get_variable('dep_crc32c')
dep_backtrace = dep_null
dep_md5 = subproject('md5').get_variable('dep_md5')
dep_brotli_enc = dependency('libbrotlienc', fallback : ['google-brotli', 'brotli_encoder_dep'])
dep_brotli_dec = dependency('libbrotlidec', fallback : ['google-brotli', 'brotli_decoder_dep'])
dep_khr = subproject('khronos').get_variable('dep_khr')
if host_machine.system() == 'windows'
  dep_mhook = subproject('mhook').get_variable('dep_mhook')
else
  dep_mhook = dep_null
endif

idep_getopt = dep_null
if cpp.get_argument_syntax() == 'msvc'
  proj_getopt = subproject('getopt')
  idep_getopt = proj_getopt.get_variable('idep_getopt')
endif

inc_common = include_directories('lib/trace', 'lib/os', 'compat')
inc_ubjson = include_directories('lib/ubjson')

subdir('lib')
subdir('dispatch')
subdir('helpers')
subdir('wrappers')
subdir('retrace')
  # subdir('scripts')
if dep_qt.found()
  # subdir('gui')
endif

dep_apitrace = declare_dependency(
  dependencies : [
    dep_glretrace_common,
    dep_image,
    dep_glproc_gl,
    dep_glhelpers,
    idep_glproc,
    dep_guids,
    dep_highlight,
    dep_image,
    dep_os,
    dep_trace,
  ]
)

if host_machine.system() != 'windows'
  subdir('cli')
endif
