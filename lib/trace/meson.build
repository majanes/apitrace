# Copyright © 2019 Intel Corporation

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

libtrace = static_library(
  'trace',
  [
    'trace_callset.cpp',
    'trace_dump.cpp',
    'trace_fast_callset.cpp',
    'trace_file.cpp',
    'trace_file_read.cpp',
    'trace_file_zlib.cpp',
    'trace_file_brotli.cpp',
    'trace_file_snappy.cpp',
    'trace_format.hpp',
    'trace_model.cpp',
    'trace_parser.cpp',
    'trace_parser_flags.cpp',
    'trace_parser_loop.cpp',
    'trace_writer.cpp',
    'trace_writer_local.cpp',
    'trace_writer_model.cpp',
    'trace_profiler.cpp',
    'trace_option.cpp',
    'trace_ostream_snappy.cpp',
    'trace_ostream_zlib.cpp',
  ],
  include_directories : [inc_common, inc_highlight, inc_guids],
  link_with : [libguids, libhighlight, libos],
  dependencies : [dep_brotli_dec, dep_zlib, dep_snappy],
  gnu_symbol_visibility : 'hidden',
)

dep_trace = declare_dependency(
  link_with : libtrace,
  include_directories : include_directories('.'),
)