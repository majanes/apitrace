# Copyright © 2019 Intel Corporation

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

_cpp_retrace = ['-DRETRACE']

_inc = include_directories(
  '../lib/highlight',
  '../helpers',
  '../dispatch',
  '../lib/image'
  # TODO; windows third party
)

glretrace_gl_cpp = custom_target(
  'glretrace_gl.cpp',
  input : 'glretrace.py',
  output : 'glretrace_gl.cpp',
  command : [prog_py, '@INPUT@'],
  capture : true,
  depend_files : [
    'retrace.py',
    '../specs/glapi.py',
    '../specs/stdapi.py',
    '../specs/gltypes.py',
  ]
)

glstate_params_cpp = custom_target(
  'glstate_params.cpp',
  input : 'glstate_params.py',
  output : 'glstate_params.cpp',
  command : [prog_py, '@INPUT@'],
  capture : true,
  depend_files : [
    '../specs/glapi.py',
    '../specs/stdapi.py',
    '../specs/gltypes.py',
  ]
)

retrace_common_sources = files(
  'json.cpp',
  'process_name.cpp',
  'retrace.cpp',
  'retrace_stdc.cpp',
  'retrace_swizzle.cpp',
  'state_writer.cpp',
  'state_writer_json.cpp',
  'state_writer_ubjson.cpp',
  'ws.cpp',
)

if host_machine.system() == 'windows'
  retrace_common_sources += files('ws_win32.cpp')
endif

libretrace_common = static_library(
  'retrace_common',
  retrace_common_sources,
  cpp_args : _cpp_retrace,
  include_directories : [_inc, inc_common, inc_ubjson],
  link_with : [libimage, libtrace],
  dependencies: [dep_mhook],
  gnu_symbol_visibility : 'hidden',
)

dep_glretrace_common = declare_dependency(
  link_with : [libretrace_common],
  include_directories : include_directories('.'),
)

if dep_x11.found()  # TODO: apple or win32
  files_glretrace = files('glws_xlib.cpp', 'glws_glx.cpp')
else
  files_glretrace = files('glws_wgl.cpp', 'glws_wgl.cpp')
endif

libglretrace_common = static_library(
  'glretrace_common',
  [
    files_glretrace,
    glproc[0],
    glretrace_gl_cpp,
    'glretrace_cgl.cpp',
    'glretrace_glx.cpp',
    'glretrace_wgl.cpp',
    'glretrace_wgl_font_bitmaps.cpp',
    'glretrace_wgl_font_outlines.cpp',
    'glretrace_egl.cpp',
    'glretrace_main.cpp',
    'glretrace_ws.cpp',
    'glstate.cpp',
    'glstate_formats.cpp',
    'glstate_images.cpp',
    glstate_params_cpp,
    'glstate_shaders.cpp',
    'glws.cpp',
    'metric_helper.cpp',
    'metric_writer.cpp',
    'metric_backend_amd_perfmon.cpp',
    'metric_backend_intel_perfquery.cpp',
    'metric_backend_opengl.cpp',
  ],
  cpp_args : _cpp_retrace,
  include_directories : [_inc, inc_common],
  link_with : [libretrace_common],
  dependencies : [dep_procps, dep_khr],
  gnu_symbol_visibility : 'hidden',
)

dep_glretrace_common = declare_dependency(
  link_with : [libglretrace_common],
  include_directories : include_directories('.'),
)

if host_machine.system() == 'windows'
  compiler = meson.get_compiler('cpp') 
  platform_deps = [compiler.find_library('winmm'),
	     compiler.find_library('psapi')]
else
  platform_deps = dep_x11
endif

glretrace = executable(
  'glretrace',
  ['retrace_main.cpp'],
  cpp_args : _cpp_retrace,
  include_directories : [_inc, inc_common],
  link_with : [
    libretrace_common, libglretrace_common, libglhelpers, libglproc_gl,
  ],
  dependencies : [dep_threads, dep_dl, dep_khr, idep_getopt, platform_deps],
  install : true,
)

if get_option('egl') and host_machine.system() != 'windows'
  if dep_waffle.found()
    files_eglretrace = files('glws_waffle.cpp')
  elif dep_x11.found()
    files_eglretrace = files('glws_xlib.cpp', 'glws_egl_xlib.cpp')
  endif
  eglretrace = executable(
    'eglretrace',
    [
      'retrace_main.cpp',
      files_eglretrace,
    ],
    cpp_args : _cpp_retrace,
    include_directories : [_inc, inc_common],
    link_with : [
      libretrace_common, libglretrace_common, libglhelpers, libglproc_gl,
    ],
    dependencies : [dep_waffle, dep_x11, dep_threads, dep_dl, dep_khr],
    install : true,
  )
endif
