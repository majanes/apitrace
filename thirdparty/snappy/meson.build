# Copyright © 2019 Intel Corporation

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

project(
  'snappy',
  'cpp'
)

cpp = meson.get_compiler('cpp')

_flags = cpp.get_supported_arguments([
  '-Wno-unused-function',
  '-fstrict-aliasing',
])

libsnappy = static_library(
  'snappy',
  ['snappy-c.cc', 'snappy-sinksource.cc', 'snappy-stubs-internal.cc', 'snappy.cc'],
  include_directories : include_directories('config'),
  cpp_flags : [_flags, '-DHAVE_CONFIG_H'],
)

dep_snappy = declare_dependency(
  link_with : libsnappy,
  include_directories : include_directories('.'),
)
